# Arithmetic operators
a = 10
b = 3

addition = a + b
subtraction = a - b
multiplication = a * b
division = a / b
modulo = a % b
exponentiation = a ** b
floor_division = a // b

print("Arithmetic Operators:")
print("Addition:", addition)
print("Subtraction:", subtraction)
print("Multiplication:", multiplication)
print("Division:", division)
print("Modulo:", modulo)
print("Exponentiation:", exponentiation)
print("Floor Division:", floor_division)

# Comparison operators
x = 5
y = 7

greater_than = x > y
less_than = x < y
equal_to = x == y
not_equal_to = x != y
greater_than_or_equal_to = x >= y
less_than_or_equal_to = x <= y

print("\nComparison Operators:")
print("Greater than:", greater_than)
print("Less than:", less_than)
print("Equal to:", equal_to)
print("Not equal to:", not_equal_to)
print("Greater than or equal to:", greater_than_or_equal_to)
print("Less than or equal to:", less_than_or_equal_to)

# Logical operators
p = True
q = False

logical_and = p and q
logical_or = p or q
logical_not_p = not p
logical_not_q = not q

print("\nLogical Operators:")
print("Logical AND:", logical_and)
print("Logical OR:", logical_or)
print("Logical NOT (p):", logical_not_p)
print("Logical NOT (q):", logical_not_q)

# Assignment operators
r = 10

r += 5
print("\nAssignment Operators:")
print("r += 5:", r)

r -= 3
print("r -= 3:", r)

r *= 2
print("r *= 2:", r)

r /= 4
print("r /= 4:", r)

r %= 3
print("r %= 3:", r)

r //= 2
print("r //= 2:", r)

r **= 3
print("r **= 3:", r)

# Bitwise operators
s = 10
t = 3

bitwise_and = s & t
bitwise_or = s | t
bitwise_xor = s ^ t
bitwise_not_s = ~s
bitwise_left_shift = s << t
bitwise_right_shift = s >> t

print("\nBitwise Operators:")
print("Bitwise AND:", bitwise_and)
print("Bitwise OR:", bitwise_or)
print("Bitwise XOR:", bitwise_xor)
print("Bitwise NOT (s):", bitwise_not_s)
print("Bitwise Left Shift:", bitwise_left_shift)
print("Bitwise Right Shift:", bitwise_right_shift)

# Membership operators
numbers = [1, 2, 3, 4, 5]

is_present = 3 in numbers
is_not_present = 6 not in numbers

print("\nMembership Operators:")
print("Is Present:", is_present)
print("Is Not Present:", is_not_present)

# Identity operators
a = 10
b = 10
c = [1, 2, 3]
d = [1, 2, 3]

is_same_a_b = a is b
is_same_c_d = c is d

print("\nIdentity Operators:")
print("Is Same (a, b):", is_same_a_b)
print("Is Same (c, d):", is_same_c_d)

# Ternary Operator
age = 20
is_adult = "Adult" if age >= 18 else "Not Adult"

print("\nTernary Operator:")
print("Is Adult?", is_adult)
